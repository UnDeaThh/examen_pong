/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    InitAudioDevice();
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //SOUNDS
    Sound damage = LoadSound("damage.wav");
    Sound choque = LoadSound("choque.wav");
    
    
    // LOGO
    Texture2D logo2 = LoadTexture("logo2.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    char logoText[20] = "MANIAC GAMES";
    int logoSize = 100;
    Font logoFont = LoadFont ("letra_logo.ttf");
    Vector2 logoPost = { screenWidth/1.5 - MeasureTextEx(logoFont, logoText, logoSize, 0).x/2, -100 };
    
    //TITLE
    char titleText[12] = "HELL'S PONG";
    char startText[30] = "PRESS START TO ENTER THE HELL";
    Font hellFont = LoadFont ("letra.ttf");
    int titleSize = 100;
    Vector2 titlePost = { screenWidth/2 - MeasureTextEx(hellFont, titleText, titleSize, 0).x/2, -100 };
    bool blink = false;
    
    //GAMEPLAY
    Texture2D fondo = LoadTexture("fondo2.png");
    
    float margenTop = 50;
    float margenBG = 8;
    float margenField = 5;
    float margenCounter = 50;
    float margenLife = 8;
    
    Rectangle bgRec = {0, margenTop, screenWidth, screenHeight - margenTop};
    Rectangle fieldRec = {bgRec.x + margenBG, bgRec.y + margenBG, bgRec.width - margenBG*2, bgRec.height - margenBG*2};
    
    Rectangle playerBgRec = {0, 0, screenWidth/2 - margenCounter, margenTop};
    Rectangle playerFillRec = {playerBgRec.x + margenLife, playerBgRec.y + margenLife, playerBgRec.width - margenLife*2, playerBgRec.height - margenLife*2};
    Rectangle playerLifeRec = playerFillRec;
    
    Rectangle enemyBgRec = {screenWidth/2 + margenCounter, 0, screenWidth/2 - margenCounter, margenTop};
    Rectangle enemyFillRec = {enemyBgRec.x + margenLife, enemyBgRec.y + margenLife, enemyBgRec.width - margenLife*2, enemyBgRec.height - margenLife*2};
    Rectangle enemyLifeRec = enemyFillRec;
    
    Rectangle player = {fieldRec.x + margenField, fieldRec.y + fieldRec.height/2 - 25, 15, 75};
    int playerSpeedY = 4;
    
    Rectangle enemy= {fieldRec.x + fieldRec.width - margenField - 15, fieldRec.y + fieldRec.height/2 - 25, 15, 75};
    int enemySpeedY = 4;
    float margenIA = enemy.height/4;
    
    Vector2 ballPosition = { fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
    Vector2 ballSpeed = { GetRandomValue(4, 5), GetRandomValue(5, 5) };
    if (GetRandomValue(0, 1)){ ballSpeed.x *= -1;}
    if (GetRandomValue(0, 1)){ ballSpeed.y *= -1;}
    
    int ballRadius = 12;
    
    int playerLife = 8;
    int enemyLife = 8;
    
    float widthDamage = playerLifeRec.width/playerLife;
    
    int secondsCounter = 99;
    
    int framesCounter = 0;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Lose, 1 - Win, 2 - Draw, -1 - Not defined
    
    bool pause = false;
    
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
                if (fadeIn){
                 alpha += 1.0f/90 ;
                    if (alpha >= 1.0f){
                        alpha = 1.0f;
                        framesCounter++;
                        if (framesCounter >= 120){
                            framesCounter = 0;
                            fadeIn = false;
                        }
                        }
                }
                else{
                    alpha -= 1.0f/90;
                    if (alpha <= 0.0f){
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                if (logoPost.y < 100){
                        logoPost.y += 2;
                    }
                    else {
                        logoPost.y = 100;
                    }
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                    if (titlePost.y < 100){
                        titlePost.y += 4;
                    }
                    else {
                        titlePost.y = 100;
                    
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                    framesCounter++;
                    if (framesCounter % 10 == 0){
                        framesCounter = 0;
                        blink = !blink;
                    }
                    }
                    if (IsKeyPressed(KEY_ENTER)){
                        framesCounter = 0;
                        screen = GAMEPLAY;
                    }
                    
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                if (!pause){
                // TODO: Ball movement logic.........................(0.2p)
                    if (ballSpeed.x > 10){
                        ballSpeed.x == 10;
                    }
                    if (ballSpeed.y > 10){
                        ballSpeed.y == 10;
                    }
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;

                // TODO: Player movement logic.......................(0.2p)
                   if (IsKeyDown(KEY_UP)){player.y -= playerSpeedY;}
                   if (IsKeyDown(KEY_DOWN)){player.y += playerSpeedY;}
                   
                   if (player.y < fieldRec.y){player.y = fieldRec.y;}
                   if (player.y > fieldRec.y + fieldRec.height - player.height){player.y = fieldRec.y + fieldRec.height - player.height;}
                
                // TODO: Enemy movement logic (IA)...................(1p)
                   if ((ballPosition.x > fieldRec.x + fieldRec.width/2) && ballSpeed.x > 0){
                        if (ballPosition.y < enemy.y + enemy.height/2 - margenIA) {enemy.y -= enemySpeedY;}
                        if (ballPosition.y > enemy.y + enemy.height/2 + margenIA) {enemy.y += enemySpeedY;}
                   }
                   if (enemy.y < fieldRec.y){enemy.y = fieldRec.y;}
                   if (enemy.y > fieldRec.y + fieldRec.height - enemy.height){enemy.y = fieldRec.y + fieldRec.height - enemy.height;}
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0){
                        PlaySound (choque);
                        ballSpeed.x *= -1;
                    }
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0){
                        PlaySound (choque);
                        ballSpeed.x *= -1;
                    }
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    //Limite TOP
                        if ((ballPosition.y - (ballRadius + 15) < fieldRec.y) && ballSpeed.y < 0){
                            ballSpeed.y *= -1;
                        }
                        //Limite BOTTOM
                        if ((ballPosition.y + ballRadius > fieldRec.y + fieldRec.height) && ballSpeed.y > 0){
                            ballSpeed.y *= -1;
                        }
                        //Limite LEFT
                        if ((ballPosition.x - ballRadius < fieldRec.x) && ballSpeed.x < 0){
                            PlaySound (damage);
                            ballSpeed.x *= -1;
                            playerLife--;
                            // TODO: Player Life bars decrease logic....................(1p)
                            playerLifeRec.width -= widthDamage;
                        }
                        //Limite RIGHT
                        if ((ballPosition.x + ballRadius > fieldRec.x + fieldRec.width) && ballSpeed.x > 0){
                            PlaySound (damage);
                            ballSpeed.x *= -1;
                            enemyLife--;
                            // TODO: Enemy Life bars decrease logic....................
                            enemyLifeRec.width -= widthDamage;
                            enemyLifeRec.x += widthDamage;
                        }
                
                

                // TODO: Time counter logic..........................(0.2p)
                    framesCounter++;
                    if (framesCounter % 60 == 0){
                        framesCounter = 0;
                        secondsCounter--;
                    }
                // TODO: Game ending logic...........................(0.2p)
                        if (secondsCounter <=0){
                            if (playerLife < enemyLife) gameResult = 0;
                            else if (playerLife > enemyLife) gameResult = 1;
                            else gameResult = 2;
                        }
                        else if (playerLife <= 0) {
                            
                            gameResult = 0;
                            
                        }
                        else if (enemyLife <= 0) gameResult = 1;
                        
                        if (gameResult != -1){
                            screen = ENDING;
                        }
                    
                
                }// TODO: Pause button logic..........................(0.2p)
                    if (IsKeyPressed(KEY_P)){
                        pause = !pause;
                    }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                    if (IsKeyPressed(KEY_ENTER)){
                        
                        ballPosition = (Vector2) { fieldRec.x + fieldRec.width/2, fieldRec.y + fieldRec.height/2};
                        ballSpeed = (Vector2) { GetRandomValue(4, 5), GetRandomValue(5, 5) };
                        if (GetRandomValue(0, 1)){ ballSpeed.x *= -1;}
                        if (GetRandomValue(0, 1)){ ballSpeed.y *= -1;}
                        
                        player.y = fieldRec.y + fieldRec.height/2 - 25;
                        enemy.y = player.y;
                        
                        playerLife = 8;
                        enemyLife = 8;
                        
                        playerLifeRec = playerFillRec;
                        enemyLifeRec = enemyFillRec;
                        
                        secondsCounter = 99;
                        framesCounter = 0;
                        
                        pause = false;
                            
                        gameResult = -1;
                        
                        
                        screen = TITLE;
                    }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(BLACK);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo2, screenWidth/2 - logo2.width/2, (screenHeight/2 - logo2.height/2) + 100, Fade(WHITE, alpha));
                    DrawTextEx (logoFont, logoText, logoPost , (logoSize)/1.5, 0, ORANGE);
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                        DrawTextEx (hellFont, titleText, titlePost, titleSize, 0, RED);
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                        if (blink) {DrawText (startText, screenWidth/2 - MeasureText(startText, 30)/2, screenHeight - 100, 30, GREEN);}
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                        DrawRectangleRec (bgRec, BLACK);
                        DrawTexture(fondo, 0, 0, WHITE);
                        
                        DrawCircleV(ballPosition, ballRadius, BLACK);
                        
                    // TODO: Draw player and enemy...................(0.2p)
                        DrawRectangleRec (player, BLACK);
                        DrawRectangleRec (enemy, BLACK);
                    // TODO: Draw player and enemy life bars.........(0.5p)
                        DrawRectangleRec (playerBgRec, DARKBLUE);
                        DrawRectangleRec (playerFillRec, RED);
                        DrawRectangleRec (playerLifeRec, YELLOW);
                        
                        DrawRectangleRec (enemyBgRec, DARKBLUE);
                        DrawRectangleRec (enemyFillRec, RED);
                        DrawRectangleRec (enemyLifeRec, YELLOW);
                    // TODO: Draw time counter.......................(0.5p)
                        DrawText(FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter), 40)/2, margenTop - 30, 40, YELLOW);
                    // TODO: Draw pause message when required........(0.5p)
                        if (pause){
                        DrawRectangle(0, 0, screenWidth, screenHeight, Fade(WHITE, 0.8f));
                        DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 100)/2, screenHeight/2 - 15, 100, BLACK);}
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                        if (gameResult == 1) DrawText ("THE SKY HAS WON THE BATTLE!", (screenWidth/2 - MeasureText("THE SKY HAS WON THE BATTLE!", 40)/2) - 25, 150, 45, GREEN);
                        else if (gameResult == 0) DrawText ("THE HELL DOMINATES THE SKY...", (screenWidth/2 - MeasureText("THE HELL DOMINATES THE SKY...", 40)/2) - 20, 150, 45, RED);
                        else if (gameResult == 2) DrawText ("THE BATTLE WILL CONTINUE ANOTHER DAY..", (screenWidth/2 - MeasureText("THE BATTLE WILL CONTINUE ANOTHER DAY..", 40)/2) + 100, 150, 30, BLUE); 
                        
                        DrawText ("PRESS ENTER TO PLAY AGAIN", screenWidth/2 - MeasureText("PRESS ENTER TO PLAY AGAIN", 40)/2, screenHeight - 75, 40, WHITE);
                        DrawText ("PRESS ESC TO EXIT", screenWidth/2 - MeasureText("PRESS ESC TO EXIT", 40)/2, screenHeight - 40, 40, WHITE);
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo2);
    UnloadFont(hellFont);
    UnloadFont(logoFont);
    UnloadTexture(fondo);
    CloseAudioDevice();
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}